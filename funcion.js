var form= document.getElementById("form");
var inputs = document.querySelectorAll('#form input');
var adver =document.getElementById("mensaje");
var adver1 =document.getElementById("resultado1");
var adver2 =document.getElementById("resultado2");
const expresionesRegulares = {
    numeros : /([0-9])/,
    text : /([a-zA-Z])/,
    caracteres : /^[^a-zA-Z\d\s]+$/,
    correo : /^([a-z\d]+[@]+[a-z]+\.[a-z]{2,})+$/,
    espacios : /\s/g
}

inputs.forEach(input=>{
    input.addEventListener('keyup', (e) => {   
        let valueInput = e.target.value;
        switch(e.target.id){

            case 'number1':
                input.value = valueInput.replace(expresionesRegulares.text, '').replace(expresionesRegulares.espacios, '');
            break;

            case 'number2':
                input.value = valueInput.replace(expresionesRegulares.text, '').replace(expresionesRegulares.espacios, '');
                
            break;

        }
    })
});

form.addEventListener("submit", e=>{
    e.preventDefault();
    var num1 =document.getElementById("number1");
    var num2 =document.getElementById("number2");
    var sumpar=0, sumimpar=0, i=0;
    if ( num1.value <= num2.value ) {

        for (i = num1.value; i<= num2.value; i++) {
            if((i%2) ==0){
                sumpar= parseInt( sumpar+i);
            }else {
                sumimpar= parseInt( sumimpar + i);
            }
        }
        adver1.innerHTML = 'La suma impar es ' + sumimpar;
        adver2.innerHTML = 'La suma par es' +sumpar;
    }else{
        adver.innerHTML = 'El numero 2 debe ser mayor';
    }
})